from django.db import models


from dirtyfields import DirtyFieldsMixin


class Contract(DirtyFieldsMixin, models.Model):
    ON_HOLD = 1
    IN_PROGRESS = 2
    DONE = 3

    CONTARCT_STATUSES = (
        (ON_HOLD, 'ON_HOLD'),
        (IN_PROGRESS, 'IN_PROGRESS'),
        (DONE, 'DONE'),
    )

    FIELDS_TO_CHECK = ('status', 'name')

    status = models.IntegerField(choices=CONTARCT_STATUSES, default=ON_HOLD)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
