from rest_framework import viewsets

from contracts.api.serializers import ContractSerializer
from contracts.models import Contract


class ContractViewSet(viewsets.ModelViewSet):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
