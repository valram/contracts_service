Django==2.0.1
psycopg2==2.7.3.2
djangorestframework==3.7.7
django-activity-stream==0.6.5
django-dirtyfields==1.3