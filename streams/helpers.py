class Singleton(type):

    _instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instance


class LoggedInUser(metaclass=Singleton):

    _user = None

    def set_user(self, request):
        if request.user.is_authenticated:
            self._user = request.user

    @property
    def current_user(self):
        return self._user


class LoggedInUserMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        logged_in_user = LoggedInUser()
        logged_in_user.set_user(request)

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response
