from django.apps import AppConfig


class ContractsConfig(AppConfig):
    name = 'contracts'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Contract'))
        from . import signals

