from actstream import action
from django.db.models.signals import post_save
from django.dispatch import receiver


from contracts.models import Contract
from streams.helpers import LoggedInUser


@receiver(post_save, sender=Contract)
def contract_create_update_handler(sender, instance, created, *args, **kwargs):

    user = LoggedInUser().current_user

    if user is None:
        return

    elif 'status' in instance.get_dirty_fields():
        action.send(
            user,
            verb=f'changed to {instance.get_status_display()}',
            action_object=instance,
        )
    elif created:
        action.send(
            user,
            verb=f'created with {instance.get_status_display()}',
            action_object=instance,
        )
