from rest_framework import routers

from contracts.api.views import ContractViewSet

router = routers.SimpleRouter()
router.register(r'contracts', ContractViewSet)
urlpatterns = router.urls