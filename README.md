# Activity stream example

Actor - user, object - contract, verb - status change

### How to start

```
$ git clone 
$ cd project
$ virtualenv --python=python3.6 .env
$ source .env/bin/activate
$ pip install -r requirements.txt
```

setup local database settings in streams.settings.py

```
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver

```
- visit django_site table with pgadmin or django admin 
and change site domain name to yourdomain.com

- open page in browser

```
GET /api-auth/login/  to login
POST /api/contracts/ to create a few contracts
GET /api/contracts/  to list contracts
PUT /api/contracts/<contract_id>/ to change statuses
```

Any Streams

/activity/feed/<content_type_id>/<object_id>/json/?pretty

Model Streams

/activity/feed/<content_type_id>/json/?pretty